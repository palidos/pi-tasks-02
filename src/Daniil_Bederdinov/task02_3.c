#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main()
{
    int min = 0;
    int max = 0;
	int i = 0;

    printf("Enter range (A-B)\n");
	scanf("%i-%i",&min,&max);
	if (min < 0 || max < 0 || min>max)
	{
		printf("Wrong value \n");
		return 1;
	}
	else
	{
		for (i = min; i < max; i++)
			printf("%i ", i);
		printf("%i", i++);
		puts("");
		return 0;
	}
}